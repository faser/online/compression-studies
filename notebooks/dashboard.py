import streamlit as st
import requests
from sseclient import SSEClient
import json

# Initialize Streamlit
st.title("Live Data Dashboard")

# Initialize lists to store data points for each chart
data_points_1 = []
data_points_2 = []
data_points_3 = []
data_points_4 = []
key_list = ["Event_Compression_ratio","PayloadQueueSizeInternal","ReceiverCh0-QueueSizeGuess","SenderCh13-QueueSizeGuess"]

# Streamlit components
chart_titles = [
    st.text_input("Chart 1 Title", value=key_list[0]),
    st.text_input("Chart 2 Title", value=key_list[1]),
    st.text_input("Chart 3 Title", value=key_list[2]),
    st.text_input("Chart 4 Title", value=key_list[3])
]

charts = []

# Create line charts and store them in the 'charts' list
for chart_index in range(len(chart_titles)):
    st.subheader(chart_titles[chart_index])
    chart = st.line_chart(data_points_1, use_container_width=True)
    charts.append(chart)

# Function to update a specific chart
def update_chart(data, chart_index):
    if chart_index == 0:
        data_points_1.append(data)
        charts[chart_index].line_chart(data_points_1)
    elif chart_index == 1:
        data_points_2.append(data)
        charts[chart_index].line_chart(data_points_2)
    elif chart_index == 2:
        data_points_3.append(data)
        charts[chart_index].line_chart(data_points_3)
    elif chart_index == 3:
        data_points_4.append(data)
        charts[chart_index].line_chart(data_points_4)
    
def process(msg):
    for index,key in enumerate(key_list):
        for element in msg:
            if element["key"]==key:
                update_chart(float(element["value"]),index)

# Server-Sent Events (SSE) listener
def sse_events(url):
    messages = SSEClient(url)
    for msg in messages:
        process(json.loads(msg.data))


# Start listening to SSE events and updating the chart
sse_events('http://localhost:5000/monitoring/infoTable/eventCompressor')