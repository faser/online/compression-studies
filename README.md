# Compression  Studies

# Physics Data Compression Study Repository



Welcome to the Physics Data Compression Study Repository! This repository houses the results of an in-depth study on data compression techniques applied to physics data. The goal of this study was to evaluate various compression algorithms and their performance when applied to physics-related datasets. The repository contains performance metrics, analysis scripts, and documentation to help you understand and reproduce the study's findings.


## Introduction

Data compression plays a crucial role in managing and storing large datasets efficiently. In this study, we explore the effectiveness of various data compression algorithms in the context of physics data. Our aim is to identify compression techniques that maintain data fidelity while significantly reducing storage requirements.

## Datasets

This directory [Link](https://cernbox.cern.ch/s/XBscegp4eR4NJID) contains the physics datasets used in this study. Each dataset is provided along with a description of its origin and characteristics. We have carefully selected these datasets to represent a diverse range of physics data scenarios.

## Compressors

We have evaluated several popular data compression algorithms

- **LZ4**
- **ZSTD**
- **Zlib**

## Analysis Scripts

The `notebooks` directory contains scripts for evaluating the performance of each compressor. These scripts measure compression ratios, decompression speed etc. They can be executed on the JSON Logs collected

## Performance Metrics

We have collected a comprehensive set of performance metrics to quantify the effectiveness of each compression technique. The metrics include:

- Compression Ratio
- Compression Time
- Decompression Time


We hope this repository proves to be a valuable resource for understanding data compression techniques in the context of physics data. Feel free to explore the results, experiment with the compressors.